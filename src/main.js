import { createApp } from 'vue'
import App from './App.vue'
// import router from './router'
// // import store from './store'
// import { createPinia } from 'pinia'
// import { useGlobalStore } from '@/store/global'
// import regist from './utils'
// import { saveFnc } from '@/utils/saveFnc'
// import { message } from 'ant-design-vue'
// import {i18nFnc} from '@/i18n/index'
// import '@/assets/font/common.less'
// import '@/assets/css/antd-theme.less';
import 'ant-design-vue/dist/antd.css'

import MoreFastTable from 'more-fast-table'
import 'more-fast-table/style.css'
const app = createApp(App)
app.use(MoreFastTable)
// app.use(createPinia())
// const store = useGlobalStore()
// store.GET_MENUDATA().finally(() => {
//   app.config.globalProperties.$storage = saveFnc
//   app.config.globalProperties.$message = message
//   app.config.globalProperties.i18nFnc = i18nFnc
//   app.config.globalProperties.$getBtnAuth = function (code) {
//     let array = saveFnc.ls.get('auth_data',{}).totalAuthArr || []
//     return array.includes(code)
//   }
//   app.use(regist)
//   app.use(router)
  // app.use(i18n)
  app.mount('#jqdd-app')
// })
